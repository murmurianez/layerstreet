var activity = {
//action
    clickX:'',
    clickY:'',
    x:0,
    y:0,
    create:function(elemType){

        db.create();
        select.unfocus();
        glob.CURRENT_ID = db.dbdata.length;

        //Сдвиг элемента при создании
            var x = activity.x = activity.x + 10;
            var y = activity.y = activity.y + 10;

        switch(elemType){
			case 'body':
				db.insert(glob.CURRENT_ID,'props',{'type':'page','tag':'div','children':'true','focus':'false'});
				db.insert(glob.CURRENT_ID,'html',{'id':'page'});
				db.insert(glob.CURRENT_ID,'css',{'background-color':'#EEEEEE','background-image':'','background-position':'50% 0%','background-repeat':'no-repeat no-repeat'});
				break;
            case 'page':
                db.insert(glob.CURRENT_ID,'props',{'type':'page','tag':'div','children':'true','focus':'false'});
                db.insert(glob.CURRENT_ID,'html',{'id':'page'});
                db.insert(glob.CURRENT_ID,'css',{'position':'relative','width':'960px','height':'1280px','margin':'20px auto','background-color':'#FFFFFF'});
                break;



            case 'div':
                db.insert(glob.CURRENT_ID,"props",{"type":["div"],"tag":["div"],"children":["true"],"content":[""]});
                db.insert(glob.CURRENT_ID,"html",{"id":["div" + glob.CURRENT_ID]});
                db.insert(glob.CURRENT_ID,"css",{"position":["absolute"],"width":[400,"px"],"height":[250,"px"],"left":[x,"px"],"top":[y,"px"],"border-radius":[0,"px"],"border-width":[0,"px"],"border-style":["solid"],"border-color":["#B9BEC3"],"background-color":["rgba(59,109,150,0.1)"],"rotate":[0,"deg"]});
                break;

			case 'h2':
				db.insert(glob.CURRENT_ID,'props',{'type':['header'],'tag':['h2'],'content':['Длинный заголовок']});
				db.insert(glob.CURRENT_ID,'html',{'id':['header' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px'],'color':['#0B598A'],'margin':[0],'font-size':[26,'px'],'line-height':[26,'px'],'font-weight':['normal'],'word-wrap':['break-word'],'white-space':['pre-wrap'],'overflow':['hidden'],'font-family':['Cuprum']});
				break;
			case 'p':
				db.insert(glob.CURRENT_ID,'props',{'type':['p'],'tag':['p'],'content':['Те, кому когда-либо приходилось делать в квартире ремонт, наверное, обращали внимание на старые газеты, наклеенные под обоями. Как правило, пока все статьи не перечитаешь, ничего другого делать не можешь. Интересно же — обрывки текста, чья-то жизнь... Так же и с рыбой. Пока заказчик не прочтет всё, он не успокоится. Бывали случаи, когда дизайн принимался именно из-за рыбного текста, который, разумеется, никакого отношения к работе не имел.']});
				db.insert(glob.CURRENT_ID,'html',{'id':['p' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[360,'px'],'height':[149,'px'],'left':[x,'px'],'top':[y,'px'],'margin':[0],'font-size':[13,'px'],'line-height':[21,'px'],'color':['#333333'],'word-wrap':['break-word'],'white-space':['pre-wrap'],'overflow':['hidden'],'font-family':['Cuprum']});
				break;
			case 'a':
				db.insert(glob.CURRENT_ID,'props',{'type':['a'],'tag':['a'],'content':['Ссылка'],'resize':['false']});
				db.insert(glob.CURRENT_ID,'html',{'id':['a' + glob.CURRENT_ID],'href':['javascript:']});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px'],'color':['#0B598A'],'font-family':['Cuprum']});
				break;
			case 'button':
				db.insert(glob.CURRENT_ID,'props',{'type':['button'],'tag':['a'],'href':['javascript:'],'content':['Кнопка']});
				db.insert(glob.CURRENT_ID,'html',{'id':['button' + glob.CURRENT_ID],'href':['javascript:']});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'display':['block'],'width':[78,'px'],'height':[28,'px'],'line-height':[28,'px'],'text-align':['center'],'text-decoration':['none'],'left':[x,'px'],'top':[y,'px'],'background-color':['#0B598A'],'color':['#FFFFFF'],'border':['1px solid #B9BEC3'],'font-family':['Cuprum']});
				break;

            case 'textarea':
                db.insert(glob.CURRENT_ID,'props',{'type':['textarea'],'tag':['textarea']});
                db.insert(glob.CURRENT_ID,'html',{'id':['textarea' + glob.CURRENT_ID],'placeholder':['Lorem ipsum dolor']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[358,'px'],'height':[208,'px'],'left':[x,'px'],'top':[y,'px'],'padding':[10,'px'],'border':['1px solid #B9BEC3'],'font-size':[14,'px'],'font-family':['Cuprum']});
                break;
            case 'input':
                db.insert(glob.CURRENT_ID,'props',{'type':['input'],'tag':['input']});
                db.insert(glob.CURRENT_ID,'html',{'id':['input' + glob.CURRENT_ID],'placeholder':['Lorem ipsum dolor'],'type':['text']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[116,'px'],'height':[22,'px'],'left':[x,'px'],'top':[y,'px'],'padding-top':[2,'px'],'padding-right':[10,'px'],'padding-bottom':[2,'px'],'padding-left':[10,'px'],'font-size':[14,'px'],'font-family':['Cuprum']});
                break;
			case 'select':
				db.insert(glob.CURRENT_ID,'props',{'type':['select'],'tag':['div'],'rotate':['false']});
				db.insert(glob.CURRENT_ID,'props',{'content':[elements.select(glob.CURRENT_ID)]});
				db.insert(glob.CURRENT_ID,'html',{'id':['select' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px'],'font-size':[14,'px'],'font-family':['Cuprum']});
				break;
            case 'checkbox':
                db.insert(glob.CURRENT_ID,'props',{'type':['checkbox'],'tag':['div'],'text':['Checkbox']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.checkbox(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['checkbox' + glob.CURRENT_ID],'type':['checkbox']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px']});
                break;
            case 'radio':
                db.insert(glob.CURRENT_ID,'props',{'type':['radio'],'tag':['div'],'text':['Radio']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.radio(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['radio' + glob.CURRENT_ID],'type':['radio']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px']});
                break;

/*
 				http://www.youtube.com/watch?v=1ot4pEYuz10
				http://www.youtube.com/watch?feature=player_detailpage&v=1ot4pEYuz10
				http://www.youtube.com/watch?feature=player_detailpage&v=1ot4pEYuz10#t=58s
				<iframe width="640" height="360" src="http://www.youtube.com/embed/1ot4pEYuz10?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>

				http://vk.com/video5202009_137190802
				<iframe src="http://vk.com/video_ext.php?oid=5202009&id=137190802&hash=b6be216feddad1bc" width="607" height="360" frameborder="0"></iframe>

				https://vimeo.com/67871488
				<iframe src="http://player.vimeo.com/video/67871488" width="400" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
				https://vimeo.com/67871488#t=13
*/

			case 'video':
                db.insert(glob.CURRENT_ID,'props',{'type':['video'],'tag':['iframe']});
                db.insert(glob.CURRENT_ID,'html',{'id':['video' + glob.CURRENT_ID],'width':['640'],'height':['360'],'src':['http://www.youtube.com/watch?v=QezTaLyPTv4&feature=player_detailpage'],'frameborder':['0'],'allowfullscreen':['']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[640,'px'],'height':[360,'px'],'left':[x,'px'],'top':[y,'px']});
                break;
            case 'image':
                db.insert(glob.CURRENT_ID,'props',{'type':['image'],'tag':['img'],'resize':['proportional']});
                db.insert(glob.CURRENT_ID,'html',{'id':['image' + glob.CURRENT_ID]});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[400,'px'],'left':[x,'px'],'top':[y,'px']});
                break;
            case 'slider':
                db.insert(glob.CURRENT_ID,'props',{'type':['slider'],'tag':['div']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.slider(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['slider' + glob.CURRENT_ID],'class':['slider dragable']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[675,'px'],'height':[300,'px'],'left':[x,'px'],'top':[y,'px']});
                break;



			case 'map':
				db.insert(glob.CURRENT_ID,'props',{'type':['map'],'tag':['div']});
				db.insert(glob.CURRENT_ID,'props',{'content':[elements.map(glob.CURRENT_ID)]});
				db.insert(glob.CURRENT_ID,'html',{'id':['map' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[600,'px'],'height':[249,'px'],'left':[x,'px'],'top':[y,'px']});
				break;
            case 'chart':
                db.insert(glob.CURRENT_ID,'props',{'type':['chart'],'tag':['div']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.chart(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['chart' + glob.CURRENT_ID]});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[618,'px'],'height':[416,'px'],'left':[x,'px'],'top':[y,'px']});
                break;
        }
		db.insert(glob.CURRENT_ID,"props",{"id":[glob.CURRENT_ID]});
        db.insert(glob.CURRENT_ID,"props",{"parent":[glob.PARENT_ID]});

        var result = '';
        for(var prop in db.select([glob.CURRENT_ID],'html')){
            if(db.select([glob.CURRENT_ID],'html',[prop]) !== undefined||''){
                result = result + ' ' + prop + '="' + db.select([glob.CURRENT_ID],'html',[prop]) + '"';
            }
        }

        var tag = db.select([glob.CURRENT_ID],'props',['tag']);
        var elem =  '<' + tag + ' data-id="' + glob.CURRENT_ID + '"' + result + ' class="dragable">' + db.select([glob.CURRENT_ID],'props',['content']) + '</' + tag + '>';
        glob.PARENT.append(elem);

        view.draw();
        view.css();
        view.html();
		select.focus();

		$('#element-name').attr('placeholder',function(){return db.select([glob.CURRENT_ID],'html',['id'])}).focus();

		ls.load();
    },

	addImage:function(event){
		var files = event.target.files; // FileList object

		// Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {

			// Only process image files.
			if (!f.type.match('image.*')) {
				continue;
			}

			var reader = new FileReader();

			reader.onload = (function(theFile) {
				return function(event) {
					activity.create('image');
					db.insert(glob.CURRENT_ID,'html',{'src':[event.target.result]});
					view.draw();
				};
			})(f);

			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
		addImagePopup.hide();
	},

	addVideo:function(link){
		activity.create('video');
		db.insert(glob.CURRENT_ID,'props',{'content':[link]});
		addVideoPopup.hide();
		view.draw();
	},

	rename:function(name){
		db.insert(glob.CURRENT_ID,'html',{'id':[name]});
		view.css();
		view.html();
	},

    edit:function(){
        if($('.darkness').length == 0){
            glob.EDITOR.append('<div class="darkness"></div>');
            glob.PARENT_ID = glob.CURRENT_ID;
			select.unfocus();
        }
    },

	stopEdit:function(){
		$('.darkness').remove();
		glob.PARENT_ID = '';
	},

	copy:function(){
		//Костыль: сделать нормальное условие
		if(!glob.CURRENT_ID){return false;}

		db.copy(glob.CURRENT_ID);
		glob.CURRENT_ID = db.dbdata.length;

		var type = db.select([glob.CURRENT_ID],'props',['type']);
		db.insert(glob.CURRENT_ID,'html',{'id':[type + glob.CURRENT_ID]});

		var result = '';
		for(var prop in db.select([glob.CURRENT_ID],'html')){
			if(db.select([glob.CURRENT_ID],'html',[prop]) !== undefined||''){
				result = result + ' ' + prop + '="' + db.select([glob.CURRENT_ID],'html',[prop]) + '"';
			}
		}

		var tag = db.select([glob.CURRENT_ID],'props',['tag']);
		var elem =  '<' + tag + ' data-id="' + glob.CURRENT_ID + '"' + result + ' class="dragable">' + db.select([glob.CURRENT_ID],'props',['content']) + '</' + tag + '>';

		glob.PARENT.append(elem);
		view.draw();
		view.css();
		view.html();
		select.focus();
		$('#element-name').attr('placeholder',function(){return db.select([glob.CURRENT_ID],'html',['id'])}).focus();
	},

    del:function(){
        if(glob.CURRENT){
            glob.CURRENT.remove();
            db.del(glob.CURRENT_ID,'');
            glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').remove();
			glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').remove();
			select.unfocus();
        }
    },

    drag:function(event,drag){
        if(drag){
            //Сдвиг по горячим клавишам
				var x,y;
                if(drag.horizontal !== undefined){
                    x = glob.CURRENT.position().left + drag.horizontal
                }else{
                    x = glob.CURRENT.position().left;
                }

                if(drag.vertical !== undefined){
                    y = glob.CURRENT.position().top + drag.vertical;
                }else{
                    y = glob.CURRENT.position().top;
                }

                db.insert(glob.CURRENT_ID,'css',{'left':[x,'px'],'top':[y,'px']});
                view.draw();
                view.css();
                select.focus();
        }else{
			if(event.ctrlKey){
				activity.copy();
			}
            var shiftX = activity.clickX - glob.PAGE.find('.focus').position().left;
            var shiftY = activity.clickY - glob.PAGE.find('.focus').position().top;
			glob.BODY.find('#editor, .focus').css('cursor','move');

			var cacheFocus = document.getElementsByClassName('focus')[0].style;
			var cacheParent = document.getElementById('page');
			var cachePage = document.getElementById('page');

			var xElem, yElem = null; //Garbage Collector breaking
           	glob.BODY.on('mousemove', '#editor, .focus', function(event){

				var x = (event.pageX - shiftX)|0;
				var y = (event.pageY - shiftY)|0;

				window.requestAnimationFrame(function(){
					if(glob.SNAP_TO_GRID && !event.shiftKey){
						// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
						cacheFocus.left = (x/10|0)*10 + 'px';
						cacheFocus.top = (y/10|0)*10 + 'px';
						xElem = ((x - cacheParent.offsetLeft + cachePage.offsetLeft + 10)/10|0)*10;
						yElem = ((y - cacheParent.offsetTop + cachePage.offsetTop + 10)/10|0)*10;
					}else{
						cacheFocus.left = x + 'px';
						cacheFocus.top = y + 'px';
						xElem = x - cacheParent.offsetLeft + cachePage.offsetLeft + 10;
						yElem = y - cacheParent.offsetTop + cachePage.offsetTop + 10;
					}

					db.insert(glob.CURRENT_ID,'css',{'left':[xElem,'px'],'top':[yElem, 'px']});
					view.draw();
				});
            });

            glob.BODY.mouseup(function(){
                glob.BODY.off('mousemove');
				glob.EDITOR.find(".focus").css('cursor','default');
                view.css();
            });
        }
    },

    rotate:function(){
		glob.EDITOR.css('cursor','move');
        glob.EDITOR.mousemove(function(cursor){
			window.requestAnimationFrame(function(){
				var cx = cursor.pageX;
				var cy = cursor.pageY;

				var x = (glob.PAGE.find('.focus').offset().left)|0;
				var y = (glob.PAGE.find('.focus').offset().top)|0;
				var w = glob.PAGE.find('.focus').width();
				var h = glob.PAGE.find('.focus').height();
				var centerX = x + w / 2;
				var centerY = y + h / 2;

				var deg = -(Math.atan2(cx - centerX,cy - centerY) / Math.PI * 180 - 45)|0;

				db.insert(glob.CURRENT_ID,'css',{'rotate':[deg,'deg']});

				view.draw();
				view.css();
				formElems.write();
			});
        });

        glob.EDITOR.mouseup(function(){
            glob.EDITOR.off('mousemove');
			glob.EDITOR.css('cursor','default');
        })
    },

    resize:function(direction){
        var focusLeft = glob.PAGE.find('.focus').offset().left;
        var focusTop = glob.PAGE.find('.focus').offset().top;
        glob.EDITOR.mousemove(function(event){

            switch(direction){
                case 'top-left': glob.EDITOR.css('cursor','nw-resize'); break;
                case 'top-right': glob.EDITOR.css('cursor','ne-resize'); break;
                case 'bottom-left': glob.EDITOR.css('cursor','sw-resize'); break;
                case 'bottom-right': glob.EDITOR.css('cursor','se-resize'); break;
            }

            var cx = event.pageX;
            var cy = event.pageY;

			if(glob.SNAP_TO_GRID && !event.shiftKey){
				// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
				var width = ((cx - focusLeft - 20)/10|0)*10;
				var height = ((cy - focusTop - 20)/10|0)*10;
			}else{
				var width = cx - focusLeft - 20;
				var height = cy - focusTop - 20;
			}

			glob.PAGE.find('.focus').css({'width':width,'height':height});

            db.insert(glob.CURRENT_ID,'css',{'width':[width|0,'px'],'height':[height|0,'px']});
            view.draw();
        });

        glob.EDITOR.mouseup(function(){
            glob.EDITOR.off('mousemove');
			glob.EDITOR.css('cursor','default');
			view.css();
			formElems.write();
        })
    }
};
console.info('siteeditpro/activity');
